defmodule Gondor.Seeds.CommerceHolder do
  @moduledoc false
  alias Gondor.Organization.Schemas.CommerceWallet
  alias Gondor.Organization.Schemas.CommerceWalletHolder
  alias Gondor.Organization.Schemas.CommerceWalletHolderAccount
  alias Gondor.Repo
  alias Gondor.Seeds.ChaincodeMessage

  require Logger

  def run(%{tenant_key: tenant_key, guid: organization_guid} = _organization) do
    count = 4

    tasks =
      for i <- 0..count do
        Task.async(fn ->
          email = "#{tenant_key}#{i}consumer@accept.com"
          password = "123456789123"

          Logger.info("======> Upserting CommerceHolder #{email}")

          %{guid: commerce_wallet_holder_guid} =
            commerce_wallet_holder =
            %CommerceWalletHolder{
              alias: "@consumer.alias.#{tenant_key}.#{i}",
              email: email,
              guid: Ecto.ULID.generate(),
              organization_guid: organization_guid,
              kyc_status: "ACCEPTED",
              password_hash: Pbkdf2.add_hash(password)[:password_hash],
              tier_level: 1
            }
            |> Repo.insert!(
              on_conflict: {:replace_all_except, [:id, :guid]},
              conflict_target: :email,
              returning: true
            )

          commerce_wallet =
            %CommerceWallet{
              guid: commerce_wallet_holder_guid,
              commerce_wallet_holder_guid: commerce_wallet_holder_guid,
              balance_with_currency: Money.new(0),
              label: "wallet of <#{email}>",
              published: true
            }
            |> Repo.insert!(on_conflict: :nothing)

          %CommerceWalletHolderAccount{
            guid: Ecto.ULID.generate(),
            account_number: "123456789123",
            commerce_wallet_holder_guid: commerce_wallet_holder_guid,
            dob: "01/01/2000",
            email: email,
            first_name: "Test",
            initial_balance_limit_with_currency: Money.new(300_000),
            invite_code: "404",
            last_name: "Consumer",
            org: tenant_key,
            organization_guid: organization_guid,
            phone_number: "1234567891"
          }
          |> Repo.insert!(on_conflict: :nothing)

          ChaincodeMessage.create_commerce_wallet(commerce_wallet_holder_guid)

          {commerce_wallet_holder, commerce_wallet}
        end)
      end

    tasks
    |> Task.yield_many(:infinity)
    |> Enum.map(fn {_, {:ok, result}} -> result end)
  end
end

defmodule Gondor.Seeds.Merchant do
  @moduledoc false
  alias Gondor.Organization.Schemas.CommerceWallet
  alias Gondor.Organization.Schemas.Merchant
  alias Gondor.Organization.Schemas.MerchantApiKeys
  alias Gondor.Organization.Schemas.MerchantEmployee
  alias Gondor.Organization.Schemas.Wallet
  alias Gondor.Repo
  alias Gondor.Seeds.ChaincodeMessage

  require Logger

  def run(%{tenant_key: tenant_key, guid: organization_guid} = _organization) do
    count = 2

    tasks =
      for i <- 0..count do
        Task.async(fn ->
          email = "#{tenant_key}#{i}merchant@accept.com"
          merchant_alias = "@merchant.alias.#{tenant_key}.#{i}"
          phone_number = "11231231234"

          Logger.info("======> Upserting Merchant #{email}")

          %{guid: ecommerce_wallet_guid} =
            %Wallet{
              guid: Ecto.ULID.generate(),
              organization_guid: organization_guid,
              balance_with_currency: Money.new(0),
              label: "eCommerce wallet of <#{email}>",
              published: true,
              wallet_type: :ecommerce
            }
            |> Repo.insert!(on_conflict: :nothing)

          %{guid: merchant_guid} =
            merchant =
            %Merchant{
              guid: Ecto.ULID.generate(),
              organization_guid: organization_guid,
              account_number: "123456789",
              alias: merchant_alias,
              email: email,
              ecommerce_wallet_guid: ecommerce_wallet_guid,
              balance_limit_with_currency: Money.new(2_500_000),
              initial_balance_limit_with_currency: Money.new(2_500_000),
              invite_code: "lccu1234",
              kyc_status: "ACCEPTED",
              merchant_name: "DEV Merchant #{i} - (#{tenant_key})",
              org: tenant_key,
              phone_number: phone_number,
              business_category: "Dev Usage etc.",
              trading_business_name: "Dev Usage"
            }
            |> Repo.insert!(
              on_conflict: {:replace_all_except, [:id, :guid]},
              conflict_target: :alias,
              returning: true
            )

          %{guid: merchant_employees_guid, id: employee_id} =
            employee =
            %MerchantEmployee{
              guid: merchant_guid,
              alias: merchant_alias,
              email: email,
              first_name: "Tester",
              last_name: "Merchant",
              merchant_guid: merchant_guid,
              merchant_role: :admin,
              needs_password_reset: false,
              password_hash: Pbkdf2.add_hash("123456789123")[:password_hash],
              password_reset_at: nil,
              phone_number: phone_number,
              temp_password_used: false,
              username: email
            }
            |> Repo.insert!(
              on_conflict: {:replace_all_except, [:id, :guid]},
              conflict_target: :alias,
              returning: true
            )

          wallet =
            %CommerceWallet{
              guid: merchant_guid,
              balance_with_currency: Money.new(0),
              label: "merchant wallet of <#{email}>",
              published: true,
              inserted_by: employee_id,
              updated_by: employee_id
            }
            |> Repo.insert!(on_conflict: :nothing)

          %MerchantApiKeys{
            guid: Ecto.ULID.generate(),
            access_key:
              "#{merchant_employees_guid}.#{Base.url_encode64(:crypto.strong_rand_bytes(10))}",
            key_status: "active",
            label: "initial key",
            merchant_employees_guid: merchant_employees_guid,
            merchant_guid: merchant_guid
          }
          |> Repo.insert!(on_conflict: :nothing)

          ChaincodeMessage.create_commerce_wallet(merchant_guid)
          ChaincodeMessage.create_ecommerce_wallet(merchant_guid, ecommerce_wallet_guid)

          {merchant, employee, wallet}
        end)
      end

    tasks
    |> Task.yield_many(:infinity)
    |> Enum.map(fn {_, {:ok, result}} -> result end)
  end
end

defmodule Gondor.Seeds.ChaincodeMessage do
  @moduledoc false
  alias Gondor.PubSub.Publisher.WalletWorker

  def create_commerce_wallet(wallet_guid) do
    WalletWorker.publish(%{
      function_name: "createCommerceWallet",
      function_args: %{},
      wallet_address: wallet_guid
    })
  end

  def create_ecommerce_wallet(merchant_guid, ecommerce_wallet_guid) do
    WalletWorker.publish(%{
      function_name: "createECommerceWallet",
      function_args: %{owner_wallet_address: merchant_guid},
      wallet_address: ecommerce_wallet_guid
    })
  end
end

defmodule Gondor.Seeds.CurrencyOrder do
  @moduledoc false
  alias Gondor.CentralBank.Schemas.MintingRequest
  alias Gondor.CurrencyOrdering.Schemas.CentralBankCurrencyOrderEvaluation
  alias Gondor.CurrencyOrdering.Schemas.CurrencyOrder
  alias Gondor.CurrencyOrdering.Schemas.CurrencyOrderState
  alias Gondor.CurrencyOrdering.Schemas.FinancialInstituteCurrencyOrderEvaluation
  alias Gondor.PubSub.Publisher.TransactionWorker
  alias Gondor.Repo

  require Logger

  def run(
        organization,
        treasury_wallet,
        cb_officer,
        cb_director,
        branch_treasurer,
        amount \\ 1_250_000_000
      ) do
    Logger.info("======> Creating a Currency Order")

    currency_order =
      %CurrencyOrder{
        guid: Ecto.ULID.generate(),
        organization_id: organization.id,
        treasury_wallet_guid: treasury_wallet.guid,
        amount_with_currency: Money.new(amount),
        blockchain_asset_key: "key",
        blockchain_hash: "hash",
        confirmation_code: "123123",
        current_state: "complete",
        central_bank_approvals_required: 1,
        financial_institute_approvals_required: 1,
        inserted_by: branch_treasurer.guid
      }
      |> Repo.insert!(on_conflict: :nothing)

    %CurrencyOrderState{
      currency_order_id: currency_order.id,
      guid: Ecto.ULID.generate(),
      inserted_by: branch_treasurer.guid,
      state: "cb_review"
    }
    |> Repo.insert!()

    _central_bank_evaluation =
      %CentralBankCurrencyOrderEvaluation{
        guid: Ecto.ULID.generate(),
        approved: true,
        currency_order_id: currency_order.id,
        currency_order_signature: "random signature",
        denial_reason: nil,
        evaluation_order: 1,
        inserted_by: cb_director.id
      }
      |> Repo.insert!()

    _financial_institute_evaluation =
      %FinancialInstituteCurrencyOrderEvaluation{
        approved: true,
        currency_order_id: currency_order.id,
        currency_order_signature: "mocked_signature",
        evaluation_order: 1,
        inserted_by: branch_treasurer.guid
      }
      |> Repo.insert!()

    %CurrencyOrderState{
      guid: Ecto.ULID.generate(),
      currency_order_id: currency_order.id,
      inserted_by: branch_treasurer.guid,
      state: "fi_review"
    }
    |> Repo.insert!()

    %CurrencyOrderState{
      guid: Ecto.ULID.generate(),
      currency_order_id: currency_order.id,
      inserted_by: cb_officer.guid,
      state: "pending_mint"
    }
    |> Repo.insert!()

    perform_minting(currency_order, cb_director)
  end

  def perform_minting(currency_order, cb_director) do
    Logger.info("======> Creating a Minging Request")

    minting_request =
      %MintingRequest{
        guid: Ecto.ULID.generate(),
        currency_order_id: currency_order.id,
        current_status: "Approved",
        inserted_by_id: cb_director.id,
        updated_offline: true
      }
      |> Repo.insert!()

    {:ok, signature} =
      MintingLaptopEmulator.run(minting_request.guid, currency_order.amount_with_currency)

    :timer.sleep(1000 * 10)

    TransactionWorker.publish(%{
      function_name: "issueStockToStockWallet",
      wallet_address: "stock",
      function_args: %{
        transaction_id: minting_request.guid,
        amount: currency_order.amount_with_currency.amount,
        currency_request_id: currency_order.guid,
        minting_request_guid: minting_request.guid,
        approval_ids: [cb_director.id],
        cmd_user_id: cb_director.id,
        key_ring_id: "",
        signature: signature
      }
    })
  end
end

defmodule Universe do
  @moduledoc false
  alias Gondor.CentralBank.Schemas.CentralBankRole
  alias Gondor.CentralBank.Schemas.CentralBankUser
  alias Gondor.Organization.Schemas.BranchUserRole
  alias Gondor.Organization.Schemas.OrganizationUser
  alias Gondor.Organization.Schemas.Wallet
  alias Gondor.Repo
  alias Gondor.Seeds.CommerceHolder
  alias Gondor.Seeds.CurrencyOrder
  alias Gondor.Seeds.Merchant

  require Logger

  import Ecto.Query

  def seed_consumers(organization) do
    commerce_wallets = CommerceHolder.run(organization)
    Logger.info("======> Inserted #{Enum.count(commerce_wallets)} Commerce Holders")

    commerce_wallets
  end

  def seed_merchants(organization) do
    merchant_wallets = Merchant.run(organization)
    Logger.info("======> Inserted #{Enum.count(merchant_wallets)} Merchant Holders")

    merchant_wallets
  end

  def get_cb_fi_users(organization) do
    treasury_wallet =
      Wallet
      |> where([w], w.wallet_type == :treasury and w.organization_guid == ^organization.guid)
      |> Repo.one()

    branch_wallet =
      Wallet
      |> where([w], w.wallet_type == :branch and w.organization_guid == ^organization.guid)
      |> Repo.one()

    cb_director =
      CentralBankUser
      |> join(:inner, [u], r in CentralBankRole, on: u.central_bank_role_id == r.id)
      |> where([u, r], r.role_name == "Director")
      |> limit(1)
      |> Repo.one()

    cb_officer =
      CentralBankUser
      |> join(:inner, [u], r in CentralBankRole, on: u.central_bank_role_id == r.id)
      |> where([u, r], r.role_name == "Officer")
      |> limit(1)
      |> Repo.one()

    branch_treasurer =
      OrganizationUser
      |> join(:inner, [u], r in BranchUserRole, on: u.branch_user_role_guid == r.guid)
      |> where([u, r], r.name == "Treasurer" and u.organization_guid == ^organization.guid)
      |> limit(1)
      |> Repo.one()

    %{
      "cb_director" => cb_director,
      "cb_officer" => cb_officer,
      "branch_treasurer" => branch_treasurer,
      "treasury_wallet" => treasury_wallet,
      "branch_wallet" => branch_wallet
    }
  end

  def topup(
        %{
          "cb_director" => cb_director,
          "cb_officer" => cb_officer,
          "branch_treasurer" => branch_treasurer,
          "treasury_wallet" => treasury_wallet,
          "branch_wallet" => branch_wallet
        },
        organization,
        amount \\ 1_250_000_000
      ) do
    CurrencyOrder.run(
      organization,
      treasury_wallet,
      cb_director,
      cb_officer,
      branch_treasurer,
      amount
    )

    :timer.sleep(1000 * 30)

    Logger.info("======> Finshed Minting Setup")
    Gondor.Seeds.TreasuryToBranchTransfer.run(branch_treasurer, treasury_wallet, branch_wallet)

    :ok
  end

  def transfer(
        %{"branch_treasurer" => branch_treasurer, "branch_wallet" => branch_wallet},
        consumers,
        [{merchant, _employee, merchant_wallet} | _] = _merchants,
        organization
      ) do
    destinations =
      Enum.map(consumers, fn {commerce_wallet_holder, _commerce_wallet} ->
        commerce_wallet_holder
      end)

    Task.start(fn ->
      Logger.info("======> Deposit to Merchant Wallet just in case")

      Gondor.Seeds.Deposit.run(
        organization,
        branch_treasurer,
        branch_wallet,
        {:ok, merchant_wallet},
        100_000
      )

      Enum.map(destinations, fn cw ->
        Logger.info("======> Paying Invoice to Merchant #{inspect(merchant_wallet)}")

        for i <- 0..99 do
          Logger.info("======> Creating a eCommerce transaction: #{i}")

          Gondor.Seeds.Ecommerce.run(
            organization,
            {cw, %{guid: cw.guid}},
            {merchant, merchant, merchant_wallet},
            1
          )
        end
      end)
    end)
  end
end

Code.eval_file("priv/repo/seeds/transfers.exs")
alias Gondor.CentralBank.Schemas.StockWallet
alias Gondor.Organization.Schemas.Organization
alias Gondor.PubSub.MessagingSetup
alias Gondor.Repo
alias Gondor.Seeder.CentralBank
alias Gondor.Seeder.Tenant

require Logger

Logger.info("======> Setting up PubSub subscriptions")
MessagingSetup.setup()

Logger.info("======> Running backward compatible seeds")
:ok = CentralBank.seed(:dev)
:ok = Tenant.seed(:dev)

organizations = Repo.all(Organization)

Repo.insert!(
  %StockWallet{
    id: 1,
    guid: Ecto.ULID.generate(),
    minting_limit_with_currency: Money.new(999_999_999),
    label: "central bank stock wallet"
  },
  on_conflict: {:replace, [:minting_limit_with_currency]},
  conflict_target: :label
)

Logger.info("======> Seeding Users")

for organization <- organizations do
  case Universe.get_cb_fi_users(organization) do
    %{"branch_treasurer" => nil} ->
      Logger.info("======> Skipping #{organization.name} because it has no branch treasurer")

    cb_fi_users ->
      Universe.topup(cb_fi_users, organization, 1_250_000_000)
      :timer.sleep(1000 * 10)
      consumers = Universe.seed_consumers(organization)
      merchants = Universe.seed_merchants(organization)
      :timer.sleep(1000 * 10)
      Universe.transfer(cb_fi_users, consumers, merchants, organization)
  end
end

Logger.info("======> Finshed Universe Setup")
