defmodule Gondor.Seeds.TreasuryToBranchTransfer do
  @moduledoc false

  alias Gondor.Organization.Schemas.TreasuryTransfer
  alias Gondor.Organization.Schemas.TreasuryTransferRequest
  alias Gondor.Organization.Schemas.TreasuryTransferRequestEvaluation
  alias Gondor.Organization.Schemas.TreasuryTransferRequestState
  alias Gondor.PubSub.Publisher.TransactionWorker
  alias Gondor.Repo

  require Logger

  def run(treasurer_org_user, treasury_wallet, branch_wallet) do
    Logger.info("======> Creating a Treasury Request")

    treasury_transfer_request =
      %TreasuryTransferRequest{
        amount_with_currency: Money.new(3_500_000),
        approvals_required: 1,
        current_state: :settling_transfer,
        destination_wallet_guid: branch_wallet.guid,
        destination_wallet_label: branch_wallet.label,
        destination_wallet_type: "branch",
        guid: Ecto.ULID.generate(),
        inserted_by_user: treasurer_org_user.guid,
        organization_guid: treasurer_org_user.organization_guid,
        source_wallet_guid: treasury_wallet.guid,
        source_wallet_label: treasury_wallet.label,
        source_wallet_type: "treasury"
      }
      |> Repo.insert!()

    %TreasuryTransferRequestState{
      guid: Ecto.ULID.generate(),
      inserted_by_user: treasurer_org_user.guid,
      organization_guid: treasurer_org_user.organization_guid,
      state: :treasury_review,
      treasury_transfer_request_guid: treasury_transfer_request.guid
    }
    |> Repo.insert!()

    %TreasuryTransferRequestState{
      guid: Ecto.ULID.generate(),
      inserted_by_user: treasurer_org_user.guid,
      organization_guid: treasurer_org_user.organization_guid,
      state: :settling_transfer,
      treasury_transfer_request_guid: treasury_transfer_request.guid
    }
    |> Repo.insert!()

    approval =
      %TreasuryTransferRequestEvaluation{
        approved: true,
        guid: Ecto.ULID.generate(),
        inserted_by_user: treasurer_org_user.guid,
        organization_guid: treasurer_org_user.organization_guid,
        treasury_transfer_request_guid: treasury_transfer_request.guid,
        treasury_transfer_signature: "Mocked, not used as of now"
      }
      |> Repo.insert!()

    transfer =
      %TreasuryTransfer{
        amount_with_currency: Money.new(3_500_000),
        destination_wallet_guid: branch_wallet.guid,
        destination_wallet_type: "branch",
        guid: Ecto.ULID.generate(),
        inserted_by_user: treasurer_org_user.guid,
        organization_guid: treasurer_org_user.organization_guid,
        source_wallet_guid: treasury_wallet.guid,
        source_wallet_type: "treasury",
        treasury_transfer_request_guid: treasury_transfer_request.guid
      }
      |> Repo.insert!()

    Repo.update!(
      Ecto.Changeset.change(treasury_wallet,
        balance_with_currency:
          Money.subtract(treasury_wallet.balance_with_currency, Money.new(3_500_000))
      )
    )

    TransactionWorker.publish(%{
      function_name: "transferFromTreasuryWalletToBranchWallet",
      wallet_address: transfer.source_wallet_guid,
      function_args: %{
        treasury_transfer_request_guid: treasury_transfer_request.guid,
        transaction_id: transfer.guid,
        amount: transfer.amount_with_currency.amount,
        destination_wallet_address: transfer.destination_wallet_guid,
        approval_ids: [approval.guid],
        signature: "",
        created_at: transfer.inserted_at
      }
    })
  end
end

defmodule Gondor.Seeds.Deposit do
  @moduledoc false

  alias Gondor.Organization.Schemas.CommerceDeposit
  alias Gondor.Organization.Schemas.CommerceDepositRequest
  alias Gondor.Organization.Schemas.CommerceDepositRequestState
  alias Gondor.PubSub.Publisher.TransactionWorker
  alias Gondor.Repo

  require Logger

  def run(
        organization,
        branch_su,
        branch_wallet,
        {_commerce_wallet_holder, commerce_wallet},
        amount \\ 1_000
      ) do
    Logger.info("======> Creating a Commerce Deposit")

    Repo.update!(
      Ecto.Changeset.change(branch_wallet,
        balance_with_currency:
          Money.subtract(branch_wallet.balance_with_currency, Money.new(amount))
      )
    )

    commerce_deposit =
      Repo.insert!(%CommerceDeposit{
        amount_with_currency: Money.new(amount),
        branch_wallet_guid: branch_wallet.guid,
        commerce_wallet_guid: commerce_wallet.guid,
        destination_commerce_wallet_balance: Money.new(0),
        destination_funds_type: "DIGITAL_CURRENCY_WALLET",
        guid: Ecto.ULID.generate(),
        reference_id: "gnd",
        source_branch_wallet_balance: Money.new(3_500_000),
        source_funds_type: "BANK_ACCOUNT",
        tenant_key: organization.tenant_key
      })

    commerce_deposit_request =
      Repo.insert!(%CommerceDepositRequest{
        amount_with_currency: Money.new(amount),
        commerce_deposit_guid: commerce_deposit.guid,
        current_state: "settling",
        destination_commerce_wallet_guid: commerce_wallet.guid,
        guid: Ecto.ULID.generate(),
        inserted_by: branch_su.guid,
        organization_guid: branch_su.organization_guid,
        reference_id: "gnd",
        source_branch_wallet_guid: branch_wallet.guid
      })

    Repo.insert!(%CommerceDepositRequestState{
      commerce_deposit_request_guid: commerce_deposit_request.guid,
      guid: Ecto.ULID.generate(),
      state: "settling"
    })

    TransactionWorker.publish(%{
      function_name: "withdrawalFromBranchWalletToCommerceWallet",
      function_args: %{
        transaction_id: commerce_deposit.guid,
        amount: commerce_deposit.amount_with_currency.amount,
        destination_wallet_address: commerce_deposit_request.destination_commerce_wallet_guid,
        source_wallet_address: commerce_deposit_request.source_branch_wallet_guid,
        approval_ids: [branch_su.guid],
        signature: "",
        tenant_key: organization.tenant_key,
        created_at: commerce_deposit.inserted_at
      },
      wallet_address: commerce_deposit_request.source_branch_wallet_guid
    })
  end
end

defmodule Gondor.Seeds.Ecommerce do
  @moduledoc false

  alias Gondor.Organization.Schemas.PaymentDetail
  alias Gondor.Organization.Schemas.PaymentDetailState
  alias Gondor.OrganizationTenant.Api.ECommerceInvoicePaymentCreation
  alias Gondor.Repo

  require Logger

  def run(
        organization,
        {commerce_wallet_holder, commerce_wallet},
        {merchant, employee, merchant_wallet},
        amount \\ 1
      ) do
    guid = Ecto.ULID.generate()
    Logger.info("======> Creating a eCommerce transaction: #{guid}")

    invoice_request =
      Repo.insert!(%PaymentDetail{
        guid: guid,
        amount_with_currency: Money.new(amount),
        source_wallet_guid: commerce_wallet.guid,
        destination_wallet_guid: merchant_wallet.guid,
        ecommerce_wallet_guid: merchant.ecommerce_wallet_guid,
        merchant_guid: merchant.guid,
        inserted_by: merchant.guid,
        destination_employee_guid: employee.guid,
        note: "note",
        payment_id:
          "randorm payment #{Base.url_encode64(:crypto.strong_rand_bytes(5), padding: false)}",
        callback_url: "google.com",
        current_state: "created",
        type_label: "ecommerce"
      })

    _created_state =
      Repo.insert!(%PaymentDetailState{state: "created", payment_detail_id: invoice_request.id})

    ECommerceInvoicePaymentCreation.transfer_from_register_to_merchant(
      %{
        invoice_guid: invoice_request.guid,
        invoice_id: invoice_request.guid,
        amount: Money.to_string(invoice_request.amount_with_currency),
        source_wallet_guid: invoice_request.source_wallet_guid,
        destination_wallet_guid: invoice_request.destination_wallet_guid,
        destination_address: invoice_request.destination_wallet_guid,
        source_employee_guid: "",
        transaction_type: "ecommerce"
      },
      commerce_wallet_holder,
      organization
    )
  end
end
