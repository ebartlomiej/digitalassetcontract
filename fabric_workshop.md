## Hyperledger Fabric Workshop

---

## Introduction
This presentation details the steps involved in setting up and interacting with a Hyperledger Fabric network, focusing on each component and the associated commands.

---

## Hyperledger Fabric Components
- **Fabric CA**: The Certificate Authority for managing identities and issuing certificates.
- **Orderer**: Responsible for ordering transactions into blocks and distributing them to peers.
- **Peer**: Maintains the ledger, validates transactions, and endorses transactions.
- **Chaincode (Smart Contract)**: Business logic for transactions, installed, and run on peers.
- **Channel**: A private communication mechanism for the members.

---

## Crypto Material Generation
- Utilize `cryptogen` to generate cryptographic material for organizations and users.
  ```bash
  cryptogen generate --config=/workspace/central-bank-crypto-config.yaml
  ```

---

##  crypto-config.yaml
```yaml
OrdererOrgs:
  - Name: Orderer
    Domain: central-bank.local
    Specs:
      - Hostname: orderer
  
PeerOrgs:
  - Name: BANK
    Domain: peer.central-bank.local
    Specs:
    - Hostname: Peer
      CommonName: "peer.central-bank.local"
    EnableNodeOUs: true
    Users:
      Count: 1
```

---

## Genesis Block and Channel Configuration
- Genesis Block for Orderer:
  ```bash
  configtxgen -profile BITTOrdererBootstrap -channelID genesischannel -outputBlock ./channel-artifacts/genesis.block -configPath .
  ```
- Channel Block for Peer:
  ```bash
  configtxgen -profile BITTInternalChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID universe -configPath .
  ```

Explanation:
- `BITTInternalChannel`: The profile used for generating the update.
- `BITTMSP`: The organization that the anchor peer belongs to.
- Other flags specify output, channel ID, and configuration path.

---

## Anchor Peer Update
```bash
configtxgen -profile BITTInternalChannel -outputAnchorPeersUpdate ./channel-artifacts/BITTMSPanchors.tx -channelID universe -asOrg BITTMSP -configPath .
```

---

## Exploring Peer Commands
Peers join channels to participate in the network. Channels are the subnet of communication between organizations.

- Listing Channels:
  ```bash
  peer channel list
  ```
- Creating a Channel:
  ```bash
  peer channel create --tls true --cafile "$ORDERER_CA_FILE" -o orderer.central-bank.local:7050 -c universe -f /etc/hyperledger/configtx/channel.tx
  ```
- Joining a Channel:
  ```bash
  peer channel join -b universe.block
  ```

Explanation:
- `--tls true`: Enable TLS for secure communication.
- `--cafile`: Specifies the path to the CA certificate.
- `-o`: Specifies the orderer address.
- `-c`: Specifies the channel name.
- `-f`: Specifies the channel configuration file.

---

## Chaincode Installation and Upgrades
- Installing Chaincode:
  ```bash
  peer chaincode install \
       -n ${CHAINCODE_NAME} \
       -v ${VERSION} \
       -l java \
       -p /workspace
  ```
- Upgrading Chaincode:
  ```bash
  peer chaincode upgrade \
       -C ${CHANNEL} \
       -n ${CHAINCODE_NAME} \
       -l java \
       -v ${VERSION} \
       -c '{"Args":[]}' \
          --tls true \
    --cafile "$ORDERER_CA_FILE"
  ```

---

## Chaincode Instantiation
```bash
peer chaincode instantiate \
     -C ${CHANNEL} \
     -n ${CHAINCODE_NAME} \
     -l java \
     -v ${VERSION} \
     -c '{"Args":[]}' \
                --tls true \
    --cafile "$ORDERER_CA_FILE"
```

```bash
# Listing installed chaincodes
peer chaincode list --installed
```

---

## Chaincode Interaction
- Querying a Chaincode:
  ```bash
  peer chaincode query -C universe -n commerce -c '{"Args":["getBranchWallet", "01HAVH9HTTPSNFP8QFZQK10PHD"]}'

  peer chaincode invoke -C universe -n commerce -c '{"Args":["createTransaction", "arg1", "arg2"]}'
  ```

Explanation:
- `-C`: Specifies the channel name.
- `-n`: Specifies the chaincode name.
- `-c`: Specifies the chaincode argument in JSON format.


---

## Transaction Flow in Hyperledger Fabric
Understanding the flow of transactions from proposal to commitment.

1. **Proposal:** Clients send transaction proposals to endorsing peers.
2. **Endorsement:** Endorsing peers simulate transaction and return endorsement if valid.
3. **Ordering:** Transactions are ordered and grouped into blocks by the orderer.
4. **Validation and Commitment:** Peers validate transactions and update ledger and World State.

---

## Slide 4: Orderer
The Orderer is responsible for ordering transactions into blocks. It receives transactions from client applications, orders them to ensure consistency, and creates blocks to be delivered to peers.

Orderer Types:
- Solo (development)
- Kafka
- Raft (production)

---

## Slide 5: Peer
Peers are network nodes that hold the ledger and smart contracts (chaincodes). They endorse transactions, maintain the ledger, and run chaincodes to execute transactions.

Peer Operations:
- Joining channels
- Installing chaincodes
- Endorsing transactions

---

## Utilizing Fabric CA
Fabric CA is pivotal for client apps to sign proposals using identities registered through it.
- Example: Client fetching keys from CA server, signing a proposal, and sending it to the peer.

Fabric CA is crucial for identity management within the network.

- **Registration:** Registering new identities with specific roles.
- **Enrollment:** Enrolling an identity to obtain an enrollment certificate (ECert).
- **Identity Management:** Managing identities and their access control within the network.

---
## Client Identity and Fabric CA
Client applications interact with the network using identities registered with Fabric CA.
- Registration: Register a new identity with Fabric CA.
- Enrollment: Get the enrollment certificate (ECert) for the registered identity.

---

## Registering and Enrolling an Identity

```bash
# Set Fabric CA Client Home
export FABRIC_CA_CLIENT_HOME=/path/to/fabric-ca-client-home

# Register a new user
fabric-ca-client register --id.name user1 --id.secret user1pw --id.type user --id.affiliation org1.department1

# Enroll the new user
fabric-ca-client enroll -u http://user1:user1pw@ca.example.com:7054
```

---

## Using an Identity Certificate in Peer Commands

```bash
# Set the MSP config path to the user's MSP directory
export CORE_PEER_MSPCONFIGPATH=/path/to/users/msp

# Execute a chaincode query as the user
peer chaincode query -C universe -n commerce -c '{"Args":["getBranchWallet", "01HAVH9HTTPSNFP8QFZQK10PHD"]}'

---

## Running Fabric CA Client in Docker
- **Starting the Container:**
  ```bash
  docker run -it --name fabric-tools hyperledger/fabric-tools:latest bash
  ```
- **Accessing the Fabric CA Client:**
  Once inside the `fabric-tools` container, the `fabric-ca-client` command can be used directly:
  ```bash
  fabric-ca-client version
  ```

## Decoding a Block
- **Peer block:**
```bash
# Fetch a block
peer channel fetch 0 block1.block -c universe
peer channel fetch 1 block1.block -c universe

# Decode the block
configtxlator proto_decode --input block1.block --type common.Block --output block1.json
```
- **System Block:**
```bash
# Fetching block
peer channel fetch config system-channel-configuration.block -c $SYSTEM_CHANNEL_NAME --orderer orderer.example.com:7050
# Decoding block
configtxlator proto_decode --input system-channel-configuration.block --type common.Block --output system-channel-configuration.json
```

---

## Understanding Merkle Tree
- **Concept**: A Merkle Tree is a data structure used to efficiently prove the consistency and content of data.
- **Usage in Hyperledger**: Used for efficiently verifying transactions and blocks.
- **Hashing:** Each transaction is hashed.
- **Tree Structure:** Hashes are organized in a tree structure.
- **Root Hash:** Enables verification of all transactions in the block.

---

## World State and CouchDB
- **World State**: The current state of the ledger, storing the latest values of all keys.
- **Blockchain**: The transaction log history - Immutable history of all transactions stored by default in the mounted files system blockfile accordingly for orderer under: /var/hyperledger/production/orderer or peer: /var/hyperledger/production
- **CouchDB**: A database for storing the world state
  Navigate through databases and documents to inspect current state: `http://localhost:5984/_utils`.

---

## Building and Running the Network
1. clone universe repo
2. navigate to numa-chaincode-commerce project: `./gradlew shadowJar`
3. build: `CHANNEL=universe docker build -t "chaincode-commerce" .`
4. navigate to numa-chaincode-handler-commerce project: `./gradlew jibDockerBuild`
5. navigate to universe and run the fresh hlf network: `./prune`
6. open network logs: `docker-compose logs -f`
7. open world state database: http://localhost:5984/_utils
8. navigate to gondor and start server in dev mode: `iex -S mix phx.server`
9. if needed cleanup a database: `MIX_ENV=dev mix do ecto.drop, ecto.create, ecto.migrate`
9. run seeding script: `Code.eval_file("priv/repo/seeds/universe.exs")`
