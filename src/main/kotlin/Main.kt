import com.example.training.*
import java.math.BigInteger
import java.util.*

//HOMEWORK
// fix negative amount in deposit
// add commerce to commerce transaction type that would use common abstract class
// play around about more use case scenarios in main() function
// add and handle custom exceptions like not used InvalidTransactionException

fun main() {
    val senderStrJson = """{ "guid": "${Utils.generatePseudoGuid()}", "type": "COMMERCE", "balance": "5000" }"""
    val receiverStrJson = """{ "guid": "2", "type": "BRANCH" }"""

    val sender = Wallet.fromJson(senderStrJson)
    val receiver = Wallet.fromJson(receiverStrJson)

    try {
        BankingOperations.deposit(sender, receiver, BigInteger.TEN)
    } catch (e: Exception) {
        println("Invalid transaction: ${e.message}")
    }

    val deposit1 = BankingOperations.deposit(receiver, sender, BigInteger.TEN)
    val witdrawal1 = BankingOperations.withdrawal(sender, receiver, BigInteger.TEN)

    val transactionList = TransactionList<TransactionI>()
    transactionList.addTransaction(deposit1)
    transactionList.addTransaction(witdrawal1)

    println(sender)
    println(receiver)
    println(transactionList.getTransactions())
    println(transactionList.getWithdrawalTransactions())
    println(transactionList.filterByType(DepositTranasction::class))

    println("Are the wallet object equal? ${sender == receiver}")
    //    creates a shallow copy of the object,
    //    be careful due references may change if properties are not copied explicitly.
    val shallowCopy = sender.copy(balance = BigInteger.TEN)
    println("Are the wallet object references equal? ${sender === shallowCopy}")
    println("Are the wallet object references equal? ${sender === sender}")
    println(shallowCopy)
//    val deepCopy = Wallet(
//        guid = UUID.randomUUID().toString(),
//        type = shallowCopy.type,
//        balance = shallowCopy.balance
//    )
//    println(deepCopy)
}