package com.example.training

import java.math.BigInteger
import com.example.training.JsonSettings.Companion.json
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

enum class WalletType {
    COMMERCE, BRANCH, STOCK, TREASURY
}

@Serializable
data class WalletRequest(
    @SerialName("guid")
    val guid: String,

    @SerialName("publicKey")
    val publicKey: String
) {
    companion object {
        fun fromJson(str: String): WalletRequest {
            return Json.decodeFromString(serializer(), str)
        }
    }
}

@Serializable
data class Wallet (
    @SerialName("guid")
    val guid: String,
    @SerialName("type")
    val type: WalletType,
    @SerialName("publicKey")
    val publicKey: String,
    @SerialName("balance")
    @Serializable(with = BigIntegerSerializer::class)
    var balance: BigInteger = BigInteger.ZERO,

    val tierLevel: Int = 1
) {
    fun credit(amount: BigInteger) {
        balance += amount
    }
    fun deduct(amount: BigInteger) {
        balance -= amount
    }
    companion object {
        fun fromJson(str: String): Wallet {
            return json.decodeFromString(str)
        }
    }

    fun toJsonString(): String {
        return Json.encodeToString(serializer(), this)
    }
}

