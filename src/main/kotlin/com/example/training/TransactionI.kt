package com.example.training

import com.example.training.Wallet
import java.math.BigInteger
interface TransactionI {
    val sender: Wallet
    val receiver: Wallet
    val amount: BigInteger

    fun execute(): Pair<Wallet, Wallet>
}
