package com.example.training

import java.math.BigInteger

object AppConfig {
    private val tierRanges: Map<Int, Pair<BigInteger, BigInteger>> = mapOf(
        1 to Pair(BigInteger.ZERO, BigInteger("5000")),
        2 to Pair(BigInteger("5001"), BigInteger("20000")),
        3 to Pair(BigInteger("20001"), BigInteger("50000"))
    )

    fun getTierRange(tierLevel: Int): Pair<BigInteger, BigInteger>? = tierRanges[tierLevel]
}