package com.example.training

import java.lang.Exception
import java.math.BigInteger
import kotlin.reflect.KClass

abstract class CommonTransaction(
    override val sender: Wallet,
    override val receiver: Wallet,
    override val amount: BigInteger
) : TransactionI {
    override fun execute(): Pair<Wallet, Wallet> {
        sender.deduct(amount)
        receiver.credit(amount)

        return Pair(sender, receiver)
    }
}

data class DepositTranasction(
    override val sender: Wallet,
    override val receiver: Wallet,
    override val amount: BigInteger
) : CommonTransaction(sender, receiver, amount) {
    init {
        validate()
    }

    private fun validate() {
        if (sender.type != WalletType.BRANCH || receiver.type != WalletType.COMMERCE) {
            throw Exception("Invalid sender or receiver in Deposit tx")
        }

        if (!isEligibleForDeposit(receiver, amount)) {
            throw TierLimitExceededException("Receiver wallet is not eligible for that tx")
        }
    }

    private fun isEligibleForDeposit(wallet: Wallet, amount: BigInteger): Boolean {
        val tierRange = AppConfig.getTierRange(wallet.tierLevel)
        if (tierRange != null) {
            return amount >= tierRange.first && amount <= tierRange.second
        }
        return false
    }
}

data class WithdrawalTranasction(
    override val sender: Wallet,
    override val receiver: Wallet,
    override val amount: BigInteger
) : TransactionI {
    init {
        validate()
    }

    private fun validate() {
        if (sender.balance < amount) {
            throw InsufficientFundsException("Insufficient funds in sender wallet")
        }
    }

    override fun execute(): Pair<Wallet, Wallet> {
        receiver.deduct(amount)
        sender.credit(amount)

        return Pair(sender, receiver)
    }
}

class TransactionList<T: TransactionI> {
    private val list = mutableListOf<T>()

    fun addTransaction(transaction: T) {
        list.add(transaction)
    }

    fun getTransactions(): List<T> {
        return list
    }

    fun getWithdrawalTransactions(): List<WithdrawalTranasction> {
        return list.filterIsInstance<WithdrawalTranasction>()
    }

//  filter by any type stored in generic list
//  declaration of a generic type R that is a subtype of T. R could be Transaction or any of its subclasses.
//  it accepts a parameter clazz of type KClass<R>. KClass is Kotlin's representation of a class.
    fun <R : T> filterByType(clazz: KClass<R>): List<R> {
        return list.filter { clazz.isInstance(it) }.map { it as R }
    }
}