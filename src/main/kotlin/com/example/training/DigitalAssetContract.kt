package com.example.training

import org.hyperledger.fabric.contract.ClientIdentity
import org.hyperledger.fabric.contract.ContractInterface
import org.hyperledger.fabric.contract.Context
import org.hyperledger.fabric.contract.annotation.Contract
import org.hyperledger.fabric.contract.annotation.Default
import org.hyperledger.fabric.contract.annotation.Info
import org.hyperledger.fabric.contract.annotation.Transaction
import org.hyperledger.fabric.shim.ChaincodeException

@Contract(
    name = "DigitalAssetContract",
    info = Info(
        title = "Digital asset contract",
        description = "Smart contract for governing monetary flow",
        version = "0.0.1"
    )
)
@Default
class DigitalAssetContract : ContractInterface {
    @Transaction
    fun createCommerceWallet(ctx: Context, walletRequestJson: String): Wallet = handleContract {
        val stub = ctx.stub
        val clientGuid = getX509Name(ClientIdentity(stub))

        val walletRequest = WalletRequest.fromJson(walletRequestJson)
        val wallet = Wallet(guid = clientGuid, type = WalletType.COMMERCE, publicKey = walletRequest.publicKey)

        stub.putStringState(clientGuid, wallet.toJsonString())
        wallet
    }

    private fun getX509Name(clientIdentity: ClientIdentity) : String {
        return clientIdentity
            .x509Certificate
            .subjectX500Principal
            .name
            .split(",")
            .find { it.contains("CN=")}
            ?.split("=")
            ?.get(1)
            ?: throw ChaincodeException("X509 certificate parsing exception")
    }

    private fun <T> handleContract(body: () -> T) = try {
        body()
    } catch (e: ChaincodeException) {
        throw e
    } catch (e: Exception) {
        throw ChaincodeException(e.message, "Some unhandled cc exception", e)
    }
}