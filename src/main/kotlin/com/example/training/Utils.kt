package com.example.training

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json
import java.math.BigInteger

@Serializer(forClass = BigInteger::class)
object BigIntegerSerializer : KSerializer<BigInteger> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("BigInteger", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, obj: BigInteger) {
        encoder.encodeString(obj.toString())
    }
    override fun deserialize(decoder: Decoder): BigInteger {
        return BigInteger(decoder.decodeString())
    }
}

object Utils {
    fun generatePseudoGuid(): String {
        val chars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        return (1..10)
            .map { chars.random() }
            .joinToString("")
    }
}

class JsonSettings {
    companion object {
        val json by lazy {
            Json {
                ignoreUnknownKeys = true
                isLenient = true
                encodeDefaults = true
            }
        }
    }
}


class InsufficientFundsException(message: String) : Exception(message)
class TierLimitExceededException(message: String) : Exception(message)
class InvalidTransactionException(message: String) : Exception(message)

