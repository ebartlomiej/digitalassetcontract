import com.example.training.DepositTranasction
import com.example.training.TransactionI
import com.example.training.Wallet
import com.example.training.WithdrawalTranasction
import java.math.BigInteger

object BankingOperations {
    fun deposit(sender: Wallet, receiver: Wallet, amount: BigInteger): TransactionI {
        return DepositTranasction(sender, receiver, amount).also { it.execute() }
    }

    fun withdrawal(sender: Wallet, receiver: Wallet, amount: BigInteger): TransactionI {
        val tx = WithdrawalTranasction(sender, receiver, amount)
//        decompose a tuple
//        val (updatedSender, updatedReceiver) = tx.execute()
        return tx
    }
}