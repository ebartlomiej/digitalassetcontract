plugins {
    kotlin("jvm") version "1.9.0"
    kotlin("plugin.serialization") version "1.9.0"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    application
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven {
        url = uri("https://jitpack.io")
    }
}

dependencies {
    testImplementation(kotlin("test"))
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.0")
    implementation("com.google.code.gson:gson:2.10.1")
    implementation("org.hyperledger.fabric-chaincode-java:fabric-chaincode-shim:2.5.0")
    implementation("org.hyperledger.fabric:fabric-protos:0.3.0")
}

tasks.test {
    useJUnitPlatform()
}

tasks.shadowJar {
    archiveBaseName.set("chaincode")
    archiveVersion.set("")
    archiveClassifier.set("")

    manifest {
        attributes["Main-Class"] = "org.hyperledger.fabric.contract.ContractRouter"
    }
}

kotlin {
    jvmToolchain(8)
}

application {
    mainClass.set("org.hyperledger.fabric.contract.ContractRouter")
}